const express = require('express');
const app = express();
const http = require('http');
const port = process.env.PORT || 3020;
const socketIO = require('socket.io');

//https://evening-chamber-61598.herokuapp.com/

app.use(express.static('public'));

let server = http.createServer(app);
let io = socketIO(server);

app.get('/', function(req, res) {
    res.sendfile('index.html', {root: _dirname + '/public'})
});
let colourArray = ["#f44336", "#ff9800", "#3f51b5", "#03a9f4", "#cddc39"];
let users = [];
let typers = [];

//main socket routine
io.on('connection', (socket) => {
    console.log('new connection established');

    socket.on('disconnect', () => {
        for (let val of users) {
            if (val.connection === socket) {
                    console.log('user disconnected');
                    colourArray.push(val.color);
                    let index = users.indexOf(val);
                    users.splice(index, 1);
                    let admin = {_id: socket.id, name: 'Admin', color: "#bbb", message: `${val.name} has left the chat`};
                    io.emit('server message', admin)
            }
        }
    });

    socket.on('join', (name) => {
        let isConnected = false;
        for (let val of users) {
            if (val.name === name) {
                isConnected = true;
            }
        }

        if (!isConnected) {
            let user;
            if (colourArray.length > 0)
                user = {_id: socket.id, name: name, connection: socket, color: colourArray.shift(), message: ''};
            else
                user = {_id: socket.id, name: name, connection: socket, color: "#bbb", message: ''};
            users.push(user);
            io.emit('nameCompleted', name);
            let admin = {_id: socket.id, name: 'Admin', color: "#bbb", message: `Welcome to chat,  ${name}`};
            io.emit('server message', admin)
        }
        else {
            io.emit('alreadyConnected', 'Name already in use')
        }
    });

    socket.on('chat message', (msg) => {
        for (let val of users) {
            if (val.connection === socket) {
                val.message = msg;
                let curUser = {_id: socket.id, name: val.name, color: val.color, message: msg}
                io.emit('server message', curUser)
            }
        }
    })

    socket.on('get users', () => {
        let currUsers = [];
        for (let val of users) {
            currUsers.push({name: val.name, color: val.color})
        }

        io.emit('send users', currUsers)
    })

    socket.on('started typing', (name) => {
        if (!typers.includes(name))
            typers.push(name);
        let isTyping = '';
        for (let val of typers) {
            isTyping = isTyping + " " + val;
        }

        io.emit('typing', isTyping)
    })

    socket.on('finished typing', (name) => {
        let isTyping = '';

        let index = typers.indexOf(name);
        typers.splice(index, 1);

        for (let val of typers) {
            isTyping = isTyping + " " + val;
        }

        io.emit('typing', isTyping)
    })
});

server.listen(port, () => {
    console.log(`starting on port ${port}`)
});
/**
 * Created by Brandon on 2017-03-09.
 */

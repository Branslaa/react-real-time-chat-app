import React from 'react'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import TextField from 'material-ui/TextField';
import styles from './index.scss';
import {Card, CardHeader, CardText} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog'
import TopBar from './topbar'
import UserList from './user_list'
import MessageList from './message_list'

import io from 'socket.io-client'

//var socket = io(`http://localhost:3020`)

  var socket = io()// <-- use this when moving back to just node

class Chat extends React.Component {
	constructor() {
		super()
		this.state = {
			users: [],
			nameCompleted: false,
			name: '',
			open: false,
			errorMessage: '',
			message: '',
			messages: [],
			typing: [],
			currentlyTyping: ''
		}
	}

	handleInputUpdate = (event) => {
		this.setState({name: event.target.value})
	};

	onchangeMessage = (msg) => {
		socket.emit('started typing', this.state.name);
		this.setState({message: msg.target.value})
	};

	handleMessageSubmit = (e) => {
		if (e.key === 'Enter' && this.state.message !== '') {
			socket.emit('chat message', this.state.message);
			socket.emit('finished typing', this.state.name);
			this.setState({message: ''})
		}
	};

	tryJoin = () => {
		if (this.state.name !== '')
			socket.emit('join', this.state.name)
	};

	onJoined = (n) => {
		if (this.state.name === n)
			this.setState({nameCompleted: true});
		socket.emit('get users');
	};

	setError = (err) => {
		this.setState({errorMessage: err})
	};

	serverMessage = (msg) => {
		socket.emit('get users');
		var messages = [...this.state.messages, msg];
		this.setState({messages: messages});
	};

	handleTyping = (type) => {
		console.log('hello')
		this.setState({currentlyTyping: type});

	}

	componentDidMount = () => {
		socket.on('nameCompleted', this.onJoined);
		socket.on('server message', this.serverMessage);
		socket.on('alreadyConnected', this.setError);
		socket.on('send users', this.populateUsers);
		socket.on('typing', this.handleTyping);
	};

	populateUsers = (usr) => {
		// var users = [...this.state.users, usr];
		// this.setState({users: users});
		this.state.users = usr;
	}

	handleOpenDialog = () => {
		socket.emit('get users');
		this.setState({open: true});
	};

	handleCloseDialog = () => {
		socket.emit('get users');
		this.setState({open: false});
	};

	render() {
		const themeStyles = {
				floatingLabelFocusStyle: {
					color: '#FFD600',
				},
			};


		return (
			<MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
				<div>
					<TopBar viewDialog={this.handleOpenDialog}/>
					<Dialog title="Current Users" modal={false} open={this.state.open}
									onRequestClose={this.handleCloseDialog}>
						<div className={styles.usersList}>
							<UserList users={this.state.users} />
						</div>
					</Dialog>
					{ this.state.nameCompleted === false &&
					<Card style={{marginTop: '10%', marginLeft: '10%', width: '80%'}}>
						<CardText style={{textAlign: 'center'}}>
							<div>
								<TextField floatingLabelText="Enter User Name"
													 onChange={this.handleInputUpdate}
													 errorText={this.state.errorMessage}
													 underlineFocusStyle={themeStyles.floatingLabelFocusStyle}
													 floatingLabelStyle={themeStyles.floatingLabelFocusStyle}/>
							</div>
							<RaisedButton label="Join" labelColor="#000" backgroundColor="#FFD600" onClick={this.tryJoin}/>
						</CardText>
					</Card>
					}
					{ this.state.nameCompleted === true &&
						<Card style={{marginTop: '10%', marginLeft: '10%', width: '80%', height: '80%'}}>
							<spam>Currently Typing: </spam>
							{!this.state.currentlyTyping.includes(this.state.name) &&
							<span>{this.state.currentlyTyping}</span>
							}
								<CardText style={{textAlign: 'center'}}>

								<div className={styles.messagesList}>
									<MessageList messages={this.state.messages} name={this.state.name}/>
								</div>
									<TextField floatingLabelText="message . . ."
														 value={this.state.message}
														 onKeyPress={this.handleMessageSubmit}
														 onChange={this.onchangeMessage}
														 underlineFocusStyle={themeStyles.floatingLabelFocusStyle}
														 floatingLabelStyle={themeStyles.floatingLabelFocusStyle}/>
							</CardText>
						</Card>
					}
			</div>
			</MuiThemeProvider>
		)
	}
}
export default Chat

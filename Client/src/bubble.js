import React from 'react'
import styles from './index.scss'

const Bubble = (props) => {
	let date = new Date();
	let time = date.getHours() + ':' + date.getMinutes();

	return (
		<div className={styles.userBubble} style={{backgroundColor: props.color, color:'black'}}>
			<span style={{fontSize:'smaller', float: 'right', marginRight: '5px'}}>At: {time}</span>
			<br/>
			<span style={{fontSize:'smaller', fontWeight:'bold', align:'left'}}>{props.name} says:</span>
			<br/>
			<span style={{fontSize:'smaller', align:'left'}}>{props.message}</span>
		</div>
	)
}
export default Bubble

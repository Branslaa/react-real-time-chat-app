import React from 'react'
import ReactDOM from 'react-dom'
import ListItem from 'material-ui/List/ListItem'
import UserIcon from 'material-ui/svg-icons/Image/tag-faces'
class User extends React.Component {
	constructor() {
		super()

	}
	componentDidMount = () => {
		var userDOM = ReactDOM.findDOMNode(this)
		userDOM.scrollIntoView({block: "end", behavior: "smooth"})
		userDOM.blur()
	}
	render() {

		return (
			<ListItem ref='user' primaryText={this.props.user.name} leftIcon={<UserIcon color={this.props.user.color}/>} disabled={true}/>
		)
	}
}
export default User

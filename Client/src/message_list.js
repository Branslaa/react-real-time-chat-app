import React from 'react'
import List from 'material-ui/List/List'

import Message from './message'

const MessageList = (props) => {

	const messages = props.messages.map((message) => {
		return (<Message message={message} name={props.name}/>);
	});

	return (
		<List>
			{messages}
		</List>
	)
};
export default MessageList



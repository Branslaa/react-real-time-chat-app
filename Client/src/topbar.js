import React from 'react'; // pulls Component object out of React library
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import UserIcon from 'material-ui/svg-icons/Social/person-outline'
import IconButton from 'material-ui/IconButton'
const TopBar = (props) => {
	const iconStyles = {
			height: 50,
			width: 50,
			marginTop: -10
		},
		onIconClicked = () => {
			props.viewDialog(); // notify the parent
		}
	return (
		<Toolbar style={{background: '#FFD600'}}>
				<ToolbarTitle text="Let's chat!" style={{color: 'black'}}/>
				<IconButton tooltip="Connected Users"
										tooltipPosition="bottom-left"
										onClick={onIconClicked}
										iconStyle={iconStyles}
				>
					<UserIcon style={iconStyles} color='black' />
				</IconButton>

		</Toolbar>
	)
}
export default TopBar

import React from 'react'
import ReactDOM from 'react-dom'
import ListItem from 'material-ui/List/ListItem'
import Bubble from './bubble'
import Triangle from './triangle'
class Message extends React.Component {
	constructor() {
		super()
	}
	componentDidMount = () => {
		var userDOM = ReactDOM.findDOMNode(this)
		userDOM.scrollIntoView({block: "end", behavior: "smooth"})
		userDOM.blur()
	}
	render() {
		let align;
		if (this.props.name === this.props.message.name) {
			align = 0;
		}
		else
			align = '40%';
		return (
			<ListItem ref='message' style={{left: align}} disabled={true}>
				<Bubble message={this.props.message.message} color={this.props.message.color} name={this.props.message.name}/>
				<Triangle color={this.props.message.color}/>
			</ListItem>
		)
	}
}
export default Message
